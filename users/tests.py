from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from users.models import User


class UserTest(APITestCase):
    fixtures = ["001_fixture.json"]

    def setUp(self):
        self.url = reverse('user_list')
        admin = User.objects.get(email='admin@g.com')
        self.client.force_authenticate(admin)

    def test_list_of_all_users(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

