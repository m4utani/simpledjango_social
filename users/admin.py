from django.contrib import admin

from users.models import User


# Register your admins here.

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ("id", 'is_active', 'email')
