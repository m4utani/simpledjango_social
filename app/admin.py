from django.contrib import admin

from app.models import Content, UserScoreContent

# Register your models here.


@admin.register(Content)
class ContentAdmin(admin.ModelAdmin):
    list_display = ("id", 'title')
    search_fields = ('id', 'title', 'description')


@admin.register(UserScoreContent)
class UserScoreContentAdmin(admin.ModelAdmin):
    list_display = ("id", 'user', 'content', 'score')
    list_filter = ('user',)
