## Run Project

In the project directory, you can run:

 `docker compose up --build `

## Admin
Open [http://localhost:8008/admin](http://localhost:8008) to view it in the browser.
```
{
    "email":"admin@g.com",
    "password":"123"
}
```

## Api

Api docs http://127.0.0.1:8008/swagger/ 

Api Login spec : 

```
POST 127.0.0.1:8008/user/api/token/ HTTP/1.1
Content-Type: application/json

{
    "email":"admin@g.com",
    "password":"123"
}
```

Api add score : 

```
POST 127.0.0.1:8008/content/score/ HTTP/1.1
Content-Type: application/json
Accept: application/json
Authorization: Bearer {your token}

{
  "score": 5,
  "content": 1
}
```

Api list of contents : 

```
GET 127.0.0.1:8008/content/ HTTP/1.1
Accept: application/json
Authorization: Bearer {your_token}
```
